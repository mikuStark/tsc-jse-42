package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;

public interface IPropertyService {
    @NonNull String getApplicationVersion();

    @NonNull String getDeveloperName();

    @NonNull String getDeveloperEmail();
}
