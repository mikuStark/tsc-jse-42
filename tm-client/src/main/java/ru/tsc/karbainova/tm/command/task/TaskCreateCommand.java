package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.Session;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String name() {
        return "create-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create task";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final String description = TerminalUtil.nextLine();
        Session session = serviceLocator.getSession();
        serviceLocator.getTaskEndpoint().createTaskAllParam(session, name, description);
        System.out.println("[OK]");
    }

}
