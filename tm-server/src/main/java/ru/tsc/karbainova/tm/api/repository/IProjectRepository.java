package ru.tsc.karbainova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.tsc.karbainova.tm.dto.ProjectDTO;

import java.util.Date;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project" +
            "(id, name, description, status, start_date, finish_date, created, user_id)" +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{start_date}, #{finish_date}, #{created}, #{user_id})")
    void add(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("start_date") Date start_date,
            @Param("finish_date") Date finish_date,
            @Param("created") Date created,
            @Param("user_id") String user_id
    );

    @Update("UPDATE tm_project" +
            "SET name=#{name}, description=#{description}, status=#{status}, start_date=#{start_date}, " +
            "finish_date=#{finish_date}, created=#{created}, user_id=#{user_id} WHERE id = #{id}")
    void update(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("start_date") Date start_date,
            @Param("finish_date") Date finish_date,
            @Param("created") Date created,
            @Param("user_id") String user_id
    );

    @Select("SELECT * FROM tm_project WHERE id=#{id} AND user_id=#{user_id} LIMIT 1")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "start_date", property = "start_date"),
            @Result(column = "finish_date", property = "finish_date")
    })
    ProjectDTO findByIdUserId(final String userId, final String id);

    @Select("SELECT * FROM tm_project WHERE name=#{name} AND user_id=#{user_id} LIMIT 1")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "start_date", property = "start_date"),
            @Result(column = "finish_date", property = "finish_date")
    })
    ProjectDTO findByNameUserId(final String userId, final String name);

    @Delete("SELECT * FROM tm_project")
    void clear();

    @Delete("SELECT * FROM tm_project WHERE id=#{id} AND user_id=#{user_id} LIMIT 1")
    void removeByIdUserId(final String userId, final String id);

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(column = "user_id", property = "user_id"),
            @Result(column = "start_date", property = "start_date"),
            @Result(column = "finish_date", property = "finish_date")
    })
    List<ProjectDTO> findAll();
}
