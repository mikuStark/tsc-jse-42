package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.api.service.IProjectToTaskService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    private IProjectService projectService;
    private IProjectToTaskService projectToTaskService;
    private ServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(
            final ServiceLocator serviceLocator,
            final IProjectService projectService
//            final IProjectToTaskService projectToTaskService
    ) {
        this.serviceLocator = serviceLocator;
        this.projectService = projectService;
//        this.projectToTaskService = projectToTaskService;
    }

    @WebMethod
    public void addProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "project") @NonNull ProjectDTO project) {
        serviceLocator.getSessionService().validate(session);
        projectService.add(project);
    }


//    @WebMethod
//    public List<Project> findAllProjectByUserId(
//            @WebParam(name = "userId") String userId,
//            @WebParam(name = "comparator") Comparator<Project> comparator
//        ) {
//        return projectService.findAll(userId, comparator);
//    }

//    @WebMethod
//    public Project findByIdProject(
//            @WebParam(name = "session") final Session session,
//            @WebParam(name = "id") String id
//    ) {
//        serviceLocator.getSessionService().validate(session);
//        return projectService.findById(session.getUserId(), id);
//    }
//
//    @WebMethod
//    public void removeByIdProject(
//            @WebParam(name = "session") final Session session,
//            @WebParam(name = "id") String id
//    ) {
//        serviceLocator.getSessionService().validate(session);
//        projectService.removeById(session.getUserId(), id);
//    }

    @WebMethod
    public List<ProjectDTO> findAllProject(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findAll();
    }

    @WebMethod
    public void createProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name
    ) {
        serviceLocator.getSessionService().validate(session);
        projectService.create(session.getUserId(), name);
    }

    @WebMethod
    public void createProjectAllParam(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description) {
        serviceLocator.getSessionService().validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "project") @NonNull ProjectDTO project) {
        serviceLocator.getSessionService().validate(session);
        projectService.remove(session.getUserId(), project);
    }

//    @WebMethod
//    public List<Project> findAllProjectByUs(
//            @WebParam(name = "session") final Session session
//    ) {
//        serviceLocator.getSessionService().validate(session);
//        return projectService.findAll(session.getUserId());
//    }

    @WebMethod
    public ProjectDTO findByNameProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public List<TaskDTO> findTaskByProjectIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return projectToTaskService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public TaskDTO taskBindByIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return projectToTaskService.taskBindById(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public TaskDTO taskUnbindByIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return projectToTaskService.taskUnbindById(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public void removeAllTaskByProjectIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        projectToTaskService.removeAllTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void removeByIdPro(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        projectToTaskService.removeById(session.getUserId(), projectId);
    }
}
