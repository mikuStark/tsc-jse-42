package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectToTaskService {

    List<TaskDTO> findTaskByProjectId(String userId, String projectId);

    TaskDTO taskBindById(String userId, String projectId, String taskId);

    TaskDTO taskUnbindById(String userId, String projectId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    void removeById(String userId, String projectId);
}
