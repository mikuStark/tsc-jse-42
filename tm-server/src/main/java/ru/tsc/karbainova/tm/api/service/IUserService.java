package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.dto.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    void addAll(Collection<UserDTO> users);

    List<UserDTO> findAll();

    UserDTO findByLogin(String login);

    @SneakyThrows
    UserDTO findById(@NonNull String id);

    UserDTO findByEmail(@NonNull String email);

    UserDTO create(String login, String password);

    UserDTO create(String login, String password, String email);

    UserDTO updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

    UserDTO setPassword(String userId, String password);

    @SneakyThrows
    UserDTO add(UserDTO user);

    @SneakyThrows
    void clear();
}
