package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IAdminUserService;
import ru.tsc.karbainova.tm.api.service.ISessionService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint {

    private IAdminUserService userService;

    private ISessionService sessionService;

    private ServiceLocator serviceLocator;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(
            final ServiceLocator serviceLocator,
            final IAdminUserService userService,
            final ISessionService sessionService
    ) {
        this.serviceLocator = serviceLocator;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void clearUser(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        userService.clear();
    }

    @WebMethod
    public void signOutByUserId(
            @WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        sessionService.signOutByUserId(session.getUserId());
    }

    @WebMethod
    public void addAllUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "users") List<UserDTO> users) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        userService.addAll(users);
    }

    @WebMethod
    public List<UserDTO> findAllUser(@WebParam(name = "session") final SessionDTO session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.findAll();
    }

    @WebMethod
    public UserDTO findByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @WebMethod
    public UserDTO removeUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "user") UserDTO user
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.removeUser(user);
    }

//    @WebMethod
//    public void removeByLoginUser(
//            @WebParam(name = "session") final Session session,
//            @WebParam(name = "login") @NonNull String login
//    ) {
//        serviceLocator.getSessionService().validate(session, Role.ADMIN);
//        userService.removeByLogin(login);
//    }

    @WebMethod
    public UserDTO lockUserByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.lockUserByLogin(login);
    }

    @WebMethod
    public UserDTO unlockUserByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.unlockUserByLogin(login);
    }
}
