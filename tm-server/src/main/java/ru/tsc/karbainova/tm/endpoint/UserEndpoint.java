package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IUserService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint {
    private IUserService userService;
    private ServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(
            final ServiceLocator serviceLocator,
            final IUserService userService
    ) {
        this.serviceLocator = serviceLocator;
        this.userService = userService;
    }

    @WebMethod
    public UserDTO createUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password) {
        serviceLocator.getSessionService().validate(session);
        return userService.create(login, password);
    }

    @WebMethod
    public UserDTO setPasswordUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "password") String password) {
        serviceLocator.getSessionService().validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public UserDTO createUserWithEmail(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email) {
        serviceLocator.getSessionService().validate(session);
        return userService.create(login, password, email);
    }

    @WebMethod
    public UserDTO updateUserUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "firstName") String firstName,
            @WebParam(name = "lastName") String lastName,
            @WebParam(name = "middleName") String middleName) {
        serviceLocator.getSessionService().validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }
}
