package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractOwnerService<TaskDTO> implements ITaskService {

    public TaskService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> findAllTaskByUserId(String userId) {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllTaskByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<TaskDTO> collection) {
        if (collection == null) return;
        for (TaskDTO i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear();
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO add(TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(
                    task.getId(),
                    task.getName(),
                    task.getDescription(),
                    task.getStatus().toString(),
                    prepare(task.getStartDate()),
                    prepare(task.getFinishDate()),
                    task.getCreated(),
                    task.getUserId(),
                    task.getProjectId()
            );
            sqlSession.commit();
            return task;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            add(task);
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeByIdUserId(userId, task.getId());
            sqlSession.commit();
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO updateById(@NonNull String userId, @NonNull String id,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);

            TaskDTO task = taskRepository.findByIdUserId(userId, id);
            if (task == null) throw new ProjectNotFoundException();
            taskRepository.update(
                    task.getId(),
                    name,
                    description,
                    task.getStatus().toString(),
                    prepare(task.getStartDate()),
                    prepare(task.getFinishDate()),
                    task.getCreated(),
                    task.getUserId(),
                    task.getProjectId()
            );
            sqlSession.commit();
            return task;
        } catch (@NonNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findByNameUserId(userId, name);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public TaskDTO findByIdUserId(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        @NonNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NonNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findByIdUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }
}
