package ru.tsc.karbainova.tm.api.service;

import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {
    boolean checkDataAccess(String login, String password);

    @SneakyThrows
    SessionDTO add(SessionDTO session);

    SessionDTO open(String login, String password);

    SessionDTO sign(SessionDTO session);

    @SneakyThrows
    List<SessionDTO> findAll();

    List<SessionDTO> getListSessionByUserId(String userId);

    void close(SessionDTO session);

    void closeAll(SessionDTO session);

    void validate(SessionDTO session);

    void validate(SessionDTO session, Role role);

    //    void signOutByLogin(String login);
    void signOutByUserId(String userId);

}
