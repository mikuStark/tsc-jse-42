package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.dto.UserDTO;

public interface IAuthService {
    String getUserId();

    UserDTO getUser();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    void checkRole(Role... roles);
}
