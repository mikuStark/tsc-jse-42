package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.dto.UserDTO;

import java.util.List;

public class UserServiceTest {
    private UserService userService;
    private UserDTO user;
    private final String userLogin = "test";

    @Before
    public void before() {
        userService = new UserService(new ConnectionService(new PropertyService()));
        UserDTO user = new UserDTO();
        user.setLogin(userLogin);

        this.user = userService.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());

        @NonNull final UserDTO projectById = userService.findById(user.getId());
        Assert.assertNotNull(projectById);
    }


    @Test
    public void findAll() {
        @NonNull final List<UserDTO> users = userService.findAll();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findById() {
        Assert.assertNotNull(userService.findById(user.getId()));
    }

    @Test
    public void findByLogin() {
        Assert.assertNotNull(userService.findByLogin(user.getLogin()));
    }
}
