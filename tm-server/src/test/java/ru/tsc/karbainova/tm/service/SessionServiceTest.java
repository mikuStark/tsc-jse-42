package ru.tsc.karbainova.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.component.Bootstrap;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.enumerated.Role;


public class SessionServiceTest {
    private SessionService sessionService;
    private SessionDTO session;
    private Bootstrap serviceLocator = new Bootstrap();
    private UserDTO user;
    private final String userLogin = "admin";

    @Before
    public void before() {
        sessionService = new SessionService(new ConnectionService(new PropertyService()));
        user = serviceLocator.getAdminUserService().create(userLogin, userLogin, Role.ADMIN);
        session = sessionService.open(userLogin, userLogin);

    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
    }

    @Test
    public void closeAll() {
        sessionService.closeAll(session);
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, sessionService.findAll().size());
    }
}
