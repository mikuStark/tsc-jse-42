package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.dto.ProjectDTO;

import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private ProjectService projectService;
    @Nullable
    private ProjectDTO project;
    private String userLogin = "test";

    @Before
    public void before() {
        projectService = new ProjectService(new ConnectionService(new PropertyService()));
        projectService.add(new ProjectDTO("Project"));
        project = projectService.findByName(userLogin, "Project");
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NonNull final ProjectDTO projectById = projectService.findByName(project.getUserId(), "Project");
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void findAll() {
        @NonNull final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NonNull final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByErrorUserId() {
        @NonNull final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertNotEquals(5, projects.size());
    }

    @Test
    public void findAllByName() {
        @NonNull final ProjectDTO projects = projectService.findByName(userLogin, project.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findAllByErrorName() {
        @NonNull final ProjectDTO projects = projectService.findByName(userLogin, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    public void removeById() {
        projectService.remove(userLogin, project);
        Assert.assertNull(projectService.findByName(project.getUserId(), "Project"));
    }
}
