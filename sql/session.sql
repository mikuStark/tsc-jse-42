--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-06-29 14:32:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 201 (class 1259 OID 16406)
-- Name: tm_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_session (
    id character varying(255) NOT NULL,
    "timestamp" bigint NOT NULL,
    signature character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.tm_session OWNER TO postgres;

--
-- TOC entry 2992 (class 0 OID 16406)
-- Dependencies: 201
-- Data for Name: tm_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_session (id, "timestamp", signature, user_id) FROM stdin;
\.


--
-- TOC entry 2861 (class 2606 OID 16413)
-- Name: tm_session session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tm_session
    ADD CONSTRAINT session_pkey PRIMARY KEY (id);


-- Completed on 2021-06-29 14:32:37

--
-- PostgreSQL database dump complete
--

