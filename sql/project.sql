--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-06-29 14:33:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 16414)
-- Name: tm_project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_project (
    id character varying(250) NOT NULL,
    name character varying(250) NOT NULL,
    description character varying(250),
    status character varying(250) NOT NULL,
    start_date date,
    finish_date date,
    user_id character varying(250),
    created date
);


ALTER TABLE public.tm_project OWNER TO postgres;

--
-- TOC entry 2993 (class 0 OID 16414)
-- Dependencies: 202
-- Data for Name: tm_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_project (id, name, description, status, start_date, finish_date, user_id, created) FROM stdin;
\.


--
-- TOC entry 2861 (class 2606 OID 16421)
-- Name: tm_project project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tm_project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- TOC entry 2862 (class 2606 OID 16422)
-- Name: tm_project user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tm_project
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES public.tm_user(id);


-- Completed on 2021-06-29 14:33:06

--
-- PostgreSQL database dump complete
--

